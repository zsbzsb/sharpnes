﻿// TODO remove this file and convert into library

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpNES
{
    class Program
    {
        static void Main(string[] args)
        {
            var cart = CartridgeLoader.LoadFromFile("nestest.nes");

            var con = new NESConsole(cart);

            con.Run();
        }
    }
}
