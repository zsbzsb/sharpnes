﻿using System;
using System.Reflection;

namespace SharpNES
{
    public class Cartridge
    {
        #region Properties
        internal byte[][] ProgramROM { get; }

        internal byte[][] CharacterROM { get; }

        internal IMapper Mapper { get; }

        internal MirroringMode Mode { get; }

        internal bool SaveRAMEnabled { get; }
        #endregion

        #region Constructors
        internal Cartridge(byte[][] programROM, byte[][] characterROM, byte mapperID, MirroringMode mode, bool saveRAMEnabled)
        {
            ProgramROM = programROM;
            CharacterROM = characterROM;
            Mode = mode;
            SaveRAMEnabled = saveRAMEnabled;

            var mapperName = "SharpNES.Mappers.Mapper" + mapperID;
            var mapperType = Assembly.GetExecutingAssembly().GetType(mapperName, false, true);

            if (mapperType != null && typeof(IMapper).IsAssignableFrom(mapperType))
            {
                Mapper = (IMapper)Activator.CreateInstance(mapperType);

                Mapper.Reset(this);
            }
            else
                throw new Exception(mapperName + " is not implemented");
        }
        #endregion
    }
}
