﻿namespace SharpNES
{
    internal class RAM
    {
        #region Constants
        private const ushort RAMSize = 0x0800;
        private const ushort RAMStart = 0x0000;
        private const ushort RAMEnd = 0x1FFF;
        #endregion

        #region Variables
        private byte[] _data = null;
        #endregion

        #region CTOR
        public RAM(Bus bus)
        {
            bus.MapReadLocationRange(RAMStart, RAMEnd, Read);
            bus.MapWriteLocationRange(RAMStart, RAMEnd, Write);
        }
        #endregion

        #region Functions
        public void Reset()
        {
            _data = new byte[RAMSize];
        }

        public void Write(ushort address, byte value)
        {
            _data[address % RAMSize] = value;
        }

        public byte Read(ushort address)
        {
            return _data[address % RAMSize];
        }
        #endregion
    }
}
