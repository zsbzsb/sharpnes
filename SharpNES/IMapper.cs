﻿using System;

namespace SharpNES
{
    internal interface IMapper
    {
        void Reset(Cartridge cartridge);

        byte ReadCPU(ushort address);

        void WriteCPU(ushort address, byte value);

        byte ReadPPU(ushort address);

        void WritePPU(ushort address, byte value);
    }
}
