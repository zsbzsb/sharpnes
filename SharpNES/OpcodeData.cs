﻿using System;

namespace SharpNES
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = false)]
    internal class OpcodeData : Attribute
    {
        #region Properties
        public byte Code { get; set; } = 0x0;

        public AddressingMode Mode { get; set; } = AddressingMode.Implied;

        public byte Cycles { get; set; } = 0;

        public bool PageCrossPenalty { get; set; } = true;
        #endregion
    }
}
