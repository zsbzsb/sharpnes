﻿using System;

namespace SharpNES
{
    internal delegate byte ReadDelegate(ushort address);
    internal delegate void WriteDelegate(ushort address, byte value);

    internal class Bus
    {
        #region Variables
        private ReadDelegate[] _readLocations;
        private WriteDelegate[] _writeLocations;
        #endregion

        #region CTOR
        public Bus(int size = 0x10000)
        {
            _readLocations = new ReadDelegate[size];
            _writeLocations = new WriteDelegate[size];
        }
        #endregion

        #region Functions
        public void MapReadLocation(ushort address, ReadDelegate handler)
        {
            if (_readLocations[address] != null)
                throw new Exception("Attempt to map an already mapped address");

            _readLocations[address] = handler;
        }

        public void MapReadLocationRange(ushort startLocation, ushort endLocation, ReadDelegate handler)
        {
            MapReadLocationRange(startLocation, endLocation, new ReadDelegate[] { handler });
        }

        public void MapReadLocationRange(ushort startLocation, ushort endLocation, ReadDelegate[] handlers)
        {
            for (uint i = startLocation; i <= endLocation; i++)
            {
                MapReadLocation((ushort)i, handlers[(ushort)(i - startLocation) % handlers.Length]);
            }
        }

        public void MapWriteLocation(ushort address, WriteDelegate handler)
        {
            if (_writeLocations[address] != null)
                throw new Exception("Attempt to map an already mapped address");

            _writeLocations[address] = handler;
        }

        public void MapWriteLocationRange(ushort startLocation, ushort endLocation, WriteDelegate handler)
        {
            MapWriteLocationRange(startLocation, endLocation, new WriteDelegate[] { handler });
        }

        public void MapWriteLocationRange(ushort startLocation, ushort endLocation, WriteDelegate[] handlers)
        {
            for (uint i = startLocation; i <= endLocation; i++)
            {
                MapWriteLocation((ushort)i, handlers[(ushort)(i - startLocation) % handlers.Length]);
            }
        }

        public byte Read(ushort address)
        {
            var handler = _readLocations[address];

            if (handler != null)
                return handler(address);
            else
                throw new Exception("Unhandled bus read operation");
        }

        public void Write(ushort address, byte value)
        {
            var handler = _writeLocations[address];

            if (handler != null)
                handler(address, value);
            else
                throw new Exception("Unhandled bus write operation");
        }
        #endregion
    }
}
