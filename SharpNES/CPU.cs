﻿using System;
using System.Reflection;

namespace SharpNES
{
    using OpcodeFunction = Action<ushort, AddressingMode>;

    internal class CPU
    {
        #region Constants
        private const ushort StackOffset = 0x0100;
        private const ushort NMIVector = 0xFFFA;
        private const ushort ResetVector = 0xFFFC;
        private const ushort IRQVector = 0xFFFE;
        #endregion

        #region Variables
        private Bus _bus;

        private byte _aRegister = 0;
        private byte _xRegister = 0;
        private byte _yRegister = 0;

        private bool _negativeFlag = false;
        private bool _overflowFlag = false;
        private bool _decimalFlag = false;
        private bool _interuptFlag = true;
        private bool _zeroFlag = false;
        private bool _carryFlag = false;

        private byte _stackPointer = 0;
        private ushort _programCounter = 0;

        private uint _cycleCount = 0;
        private OpcodeFunction[] _opcodeFunctions;
        #endregion

        #region CTOR
        public CPU(Bus bus)
        {
            _bus = bus;
            LoadOpcodeFuntions();
        }
        #endregion

        #region Functions
        public void Reset()
        {
            _programCounter = Read16(0xFFFC);
            _stackPointer = 0xFD;

            SetFlags(0x04);
        }

        public uint Step()
        {
            _cycleCount = 0;

            var opcode = OpcodesLoader.GetOpcode(Advance8());

            _cycleCount += opcode.Data.Cycles;
            _opcodeFunctions[(int)opcode.Opcode](GetAddress(opcode.Data.Mode, opcode.Data.PageCrossPenalty), opcode.Data.Mode);

            return _cycleCount;
        }

        private byte Read8(ushort address)
        {
            return _bus.Read(address);
        }

        private ushort Read16(ushort address)
        {
            ushort low = Read8(address);
            ushort high = Read8((ushort)(address + 1));

            return (ushort)((high << 8) | low);
        }

        private byte Advance8()
        {
            return Read8(_programCounter++);
        }

        private ushort Advance16()
        {
            var value = Read16(_programCounter);

            _programCounter += 2;

            return value;
        }

        private byte Pop8()
        {
            return Read8((ushort)(StackOffset | ++_stackPointer));
        }

        private ushort Pop16()
        {
            var value = Read16((ushort)(StackOffset | ++_stackPointer));

            _stackPointer++;

            return value;
        }

        private void Push8(byte value)
        {
            Write8((ushort)(StackOffset | _stackPointer--), value);
        }

        private void Push16(ushort value)
        {
            Write16((ushort)(StackOffset | --_stackPointer), value);

            _stackPointer--;
        }

        private void Write8(ushort address, byte value)
        {
            _bus.Write(address, value);
        }

        private void Write16(ushort address, ushort value)
        {
            byte low = (byte)(value & 0x00FF);
            byte high = (byte)(value >> 8);

            Write8(address, low);
            Write8((ushort)(address + 1), high);
        }

        private ushort GetAddress(AddressingMode mode, bool pageCrossPenalty)
        {
            // http://wiki.nesdev.com/w/index.php/CPU_addressing_modes
            // http://www.thealmightyguru.com/Games/Hacking/Wiki/index.php?title=Addressing_Modes

            ushort address = 0;

            switch (mode)
            {
                case AddressingMode.Implied:
                case AddressingMode.Accumulator:
                    break;

                case AddressingMode.Immediate:
                    address = _programCounter++;

                    break;

                case AddressingMode.ZeroPage:
                    address = Advance8();

                    break;

                case AddressingMode.ZeroPageX:
                    address = (ushort)((Advance8() + _xRegister) % 0x100);

                    break;

                case AddressingMode.ZeroPageY:
                    address = (ushort)((Advance8() + _yRegister) % 0x100);

                    break;

                case AddressingMode.Absolute:
                    address = Advance16();

                    break;

                case AddressingMode.AbsoluteX:
                    {
                        var arg = Advance16();
                        address = (ushort)(arg + _xRegister);

                        if (pageCrossPenalty)
                            CheckPageBoundry(arg, address);

                        break;
                    }

                case AddressingMode.AbsoluteY:
                    {
                        var arg = Advance16();
                        address = (ushort)(arg + _yRegister);

                        if (pageCrossPenalty)
                            CheckPageBoundry(arg, address);

                        break;
                    }

                case AddressingMode.Indirect:
                    // Indirect addressing has a wrap-around bug
                    // JMP ($xxFF) will not work as expected
                    // the low byte will be read from $xxFF, but
                    // the high byte will be read from $xx00
                    // (not $[xx+1]00 as expected)

                    {
                        var arg = Advance16();
                        address = (ushort)(Read8(arg) | (Read8((ushort)((arg & 0xFF00) | ((arg + 1) & 0x00FF))) << 8));

                        break;
                    }

                case AddressingMode.IndirectX:
                    {
                        var arg = Advance8();
                        address = (ushort)(Read8((ushort)((arg + _xRegister) % 0x100)) + Read8((ushort)((arg + _xRegister + 1) % 0x100)) * 0x100);

                        break;
                    }

                case AddressingMode.IndirectY:
                    {
                        var arg = Advance8();
                        address = (ushort)(Read8(arg) + (Read8((ushort)((arg + 1) % 0x100)) << 8) + _yRegister);

                        if (pageCrossPenalty)
                            CheckPageBoundry((ushort)(address - _yRegister), address);

                        break;
                    }

                case AddressingMode.Relative:
                    {
                        var arg = Advance8();
                        address = (ushort)((sbyte)arg + _programCounter);

                        // No need to check the penalty boolean here because it will always be true
                        CheckPageBoundry(address, _programCounter);

                        break;
                    }

                default:
                    throw new Exception("Addressing mode " + mode.ToString() + " has no implementation");
            }

            return address;
        }

        private void CheckPageBoundry(ushort a, ushort b)
        {
            if ((a & 0xFF00) != (b & 0xFF00))
                _cycleCount += 1;
        }

        private void SetNZ(byte value)
        {
            _negativeFlag = (value & 0x80) != 0;
            _zeroFlag = value == 0;
        }

        private void Branch(ushort address)
        {
            _cycleCount += 1;
            _programCounter = address;
        }

        private void Compare(ushort address, byte value)
        {
            Compare(value, Read8(address));
        }

        private void Compare(byte a, byte b)
        {
            SetNZ((byte)(a - b));
            _carryFlag = a >= b;
        }

        private void SetFlags(byte Value)
        {
            _carryFlag = (Value & (1 << 0)) != 0;
            _zeroFlag = (Value & (1 << 1)) != 0;
            _interuptFlag = (Value & (1 << 2)) != 0;
            _decimalFlag = (Value & (1 << 3)) != 0;
            _overflowFlag = (Value & (1 << 6)) != 0;
            _negativeFlag = (Value & (1 << 7)) != 0;
        }

        private byte GetFlags()
        {
            byte flags = 0x20;

            if (_carryFlag)
                flags |= (byte)(1 << 0);

            if (_zeroFlag)
                flags |= (byte)(1 << 1);

            if (_interuptFlag)
                flags |= (byte)(1 << 2);

            if (_decimalFlag)
                flags |= (byte)(1 << 3);

            if (_overflowFlag)
                flags |= (byte)(1 << 6);

            if (_negativeFlag)
                flags |= (byte)(1 << 7);

            return flags;
        }

        #region Opcodes Implementation
        private void LoadOpcodeFuntions()
        {
            var opcodes = Enum.GetNames(typeof(Opcodes));
            _opcodeFunctions = new OpcodeFunction[opcodes.Length];

            for (int i = 0; i < opcodes.Length; i++)
            {
                var opcodeMethod = this.GetType().GetMethod("Opcode" + opcodes[i], BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.InvokeMethod | BindingFlags.Instance);

                if (opcodeMethod != null)
                    try
                    {
                        _opcodeFunctions[i] = (OpcodeFunction)Delegate.CreateDelegate(typeof(OpcodeFunction), this, opcodeMethod);
                    }
                    catch
                    {
                        throw new Exception("Failed to load the opcode function for " + opcodes[i] + "... Please ensure the method is correctly defined and the opcode is correctly defined.");
                    }
                else
                    throw new Exception("Opcode " + opcodes[i].ToString() + " has no function implementation, please ensure the function is properly defined and implemented.");
            }
        }

        private void OpcodeLDA(ushort address, AddressingMode mode)
        {
            _aRegister = Read8(address);

            SetNZ(_aRegister);
        }

        private void OpcodeLDX(ushort address, AddressingMode mode)
        {
            _xRegister = Read8(address);

            SetNZ(_xRegister);
        }

        private void OpcodeLDY(ushort address, AddressingMode mode)
        {
            _yRegister = Read8(address);

            SetNZ(_yRegister);
        }

        private void OpcodeSTA(ushort address, AddressingMode mode)
        {
            Write8(address, _aRegister);
        }

        private void OpcodeSTX(ushort address, AddressingMode mode)
        {
            Write8(address, _xRegister);
        }

        private void OpcodeSTY(ushort address, AddressingMode mode)
        {
            Write8(address, _yRegister);
        }

        private void OpcodeTAX(ushort address, AddressingMode mode)
        {
            _xRegister = _aRegister;

            SetNZ(_xRegister);
        }

        private void OpcodeTAY(ushort address, AddressingMode mode)
        {
            _yRegister = _aRegister;

            SetNZ(_yRegister);
        }

        private void OpcodeTSX(ushort address, AddressingMode mode)
        {
            _xRegister = _stackPointer;

            SetNZ(_xRegister);
        }

        private void OpcodeTXA(ushort address, AddressingMode mode)
        {
            _aRegister = _xRegister;

            SetNZ(_aRegister);
        }

        private void OpcodeTXS(ushort address, AddressingMode mode)
        {
            _stackPointer = _xRegister;
        }

        private void OpcodeTYA(ushort address, AddressingMode mode)
        {
            _aRegister = _yRegister;

            SetNZ(_aRegister);
        }

        private void OpcodeCLC(ushort address, AddressingMode mode)
        {
            _carryFlag = false;
        }

        private void OpcodeCLD(ushort address, AddressingMode mode)
        {
            _decimalFlag = false;
        }

        private void OpcodeCLI(ushort address, AddressingMode mode)
        {
            _interuptFlag = false;
        }

        private void OpcodeCLV(ushort address, AddressingMode mode)
        {
            _overflowFlag = false;
        }

        private void OpcodeSEC(ushort address, AddressingMode mode)
        {
            _carryFlag = true;
        }

        private void OpcodeSED(ushort address, AddressingMode mode)
        {
            _decimalFlag = true;
        }

        private void OpcodeSEI(ushort address, AddressingMode mode)
        {
            _interuptFlag = true;
        }

        private void OpcodeBPL(ushort address, AddressingMode mode)
        {
            if (!_negativeFlag)
                Branch(address);
        }

        private void OpcodeBMI(ushort address, AddressingMode mode)
        {
            if (_negativeFlag)
                Branch(address);
        }

        private void OpcodeBVC(ushort address, AddressingMode mode)
        {
            if (!_overflowFlag)
                Branch(address);
        }

        private void OpcodeBVS(ushort address, AddressingMode mode)
        {
            if (_overflowFlag)
                Branch(address);
        }

        private void OpcodeBCC(ushort address, AddressingMode mode)
        {
            if (!_carryFlag)
                Branch(address);
        }

        private void OpcodeBCS(ushort address, AddressingMode mode)
        {
            if (_carryFlag)
                Branch(address);
        }

        private void OpcodeBNE(ushort address, AddressingMode mode)
        {
            if (!_zeroFlag)
                Branch(address);
        }

        private void OpcodeBEQ(ushort address, AddressingMode mode)
        {
            if (_zeroFlag)
                Branch(address);
        }

        private void OpcodeDEX(ushort address, AddressingMode mode)
        {
            SetNZ(--_xRegister);
        }

        private void OpcodeDEY(ushort address, AddressingMode mode)
        {
            SetNZ(--_yRegister);
        }

        private void OpcodeINX(ushort address, AddressingMode mode)
        {
            SetNZ(++_xRegister);
        }

        private void OpcodeINY(ushort address, AddressingMode mode)
        {
            SetNZ(++_yRegister);
        }

        private void OpcodeDEC(ushort address, AddressingMode mode)
        {
            var value = (byte)(Read8(address) - 1);

            Write8(address, value);
            SetNZ(value);
        }

        private void OpcodeINC(ushort address, AddressingMode mode)
        {
            var value = (byte)(Read8(address) + 1);

            Write8(address, value);
            SetNZ(value);
        }

        private void OpcodeCMP(ushort address, AddressingMode mode)
        {
            Compare(address, _aRegister);
        }

        private void OpcodeCPX(ushort address, AddressingMode mode)
        {
            Compare(address, _xRegister);
        }

        private void OpcodeCPY(ushort address, AddressingMode mode)
        {
            Compare(address, _yRegister);
        }

        private void OpcodeJMP(ushort address, AddressingMode mode)
        {
            _programCounter = address;
        }

        private void OpcodeJSR(ushort address, AddressingMode mode)
        {
            Push16((ushort)(_programCounter - 1));

            _programCounter = address;
        }

        private void OpcodeRTI(ushort address, AddressingMode mode)
        {
            SetFlags(Pop8());

            _programCounter = Pop16();
        }

        private void OpcodeRTS(ushort address, AddressingMode mode)
        {
            _programCounter = (ushort)(Pop16() + 1);
        }

        private void OpcodeAND(ushort address, AddressingMode mode)
        {
            _aRegister &= Read8(address);

            SetNZ(_aRegister);
        }

        private void OpcodeEOR(ushort address, AddressingMode mode)
        {
            _aRegister ^= Read8(address);

            SetNZ(_aRegister);
        }

        private void OpcodeORA(ushort address, AddressingMode mode)
        {
            _aRegister |= Read8(address);

            SetNZ(_aRegister);
        }

        private void OpcodeASL(ushort address, AddressingMode mode)
        {
            if (mode == AddressingMode.Accumulator)
            {
                _carryFlag = (_aRegister >> 7) != 0;
                _aRegister <<= 1;

                SetNZ(_aRegister);
            }
            else
            {
                var value = Read8(address);
                _carryFlag = (value >> 7) != 0;
                value <<= 1;

                Write8(address, value);
                SetNZ(value);
            }
        }

        private void OpcodeLSR(ushort address, AddressingMode mode)
        {
            if (mode == AddressingMode.Accumulator)
            {
                _carryFlag = (_aRegister & 0x01) != 0;
                _aRegister >>= 1;

                SetNZ(_aRegister);
            }
            else
            {
                var value = Read8(address);
                _carryFlag = (value & 0x01) != 0;
                value >>= 1;

                Write8(address, value);
                SetNZ(value);
            }
        }

        private void OpcodeROL(ushort address, AddressingMode mode)
        {
            if (mode == AddressingMode.Accumulator)
            {
                var carry = _carryFlag;
                _carryFlag = (_aRegister >> 7) != 0;
                _aRegister = (byte)((_aRegister << 1) | (carry ? 0x01 : 0x00));

                SetNZ(_aRegister);
            }
            else
            {
                var value = Read8(address);
                var carry = _carryFlag;
                _carryFlag = (value >> 7) != 0;
                value = (byte)((value << 1) | (carry ? 0x01 : 0x00));

                Write8(address, value);
                SetNZ(value);
            }
        }

        private void OpcodeROR(ushort address, AddressingMode mode)
        {
            if (mode == AddressingMode.Accumulator)
            {
                var carry = _carryFlag;
                _carryFlag = (_aRegister & 0x01) != 0;
                _aRegister = (byte)((_aRegister >> 1) | (carry ? 0x80 : 0x00));

                SetNZ(_aRegister);
            }
            else
            {
                var value = Read8(address);
                var carry = _carryFlag;
                _carryFlag = (value & 0x01) != 0;
                value = (byte)((value >> 1) | (carry ? 0x80 : 0x00));

                Write8(address, value);
                SetNZ(value);
            }
        }

        private void OpcodeADC(ushort address, AddressingMode mode)
        {
            // Decimal Flag has no effect on the NES

            var value = Read8(address);
            var total = _aRegister + value + (_carryFlag ? 1 : 0);

            _overflowFlag = ((_aRegister ^ total) & (value ^ total) & 0x80) != 0;
            _carryFlag = total > 0xFF;
            _aRegister = (byte)total;

            SetNZ(_aRegister);
        }

        private void OpcodeSBC(ushort address, AddressingMode mode)
        {
            // Decimal Flag has no effect on the NES

            var value = Read8(address);
            var total = _aRegister - value - (_carryFlag ? 0 : 1);

            _overflowFlag = ((_aRegister ^ value) & (_aRegister ^ total) & 0x80) != 0;
            _carryFlag = total >= 0;
            _aRegister = (byte)total;

            SetNZ(_aRegister);
        }

        private void OpcodePHA(ushort address, AddressingMode mode)
        {
            Push8(_aRegister);
        }

        private void OpcodePHP(ushort address, AddressingMode mode)
        {
            Push8((byte)(GetFlags() | 0x30));
        }

        private void OpcodePLA(ushort address, AddressingMode mode)
        {
            _aRegister = Pop8();

            SetNZ(_aRegister);
        }

        private void OpcodePLP(ushort address, AddressingMode mode)
        {
            SetFlags(Pop8());
        }

        private void OpcodeBRK(ushort address, AddressingMode mode)
        {
            Push16(_programCounter);
            Push8((byte)(GetFlags() | 0x30));

            _interuptFlag = true;
            _programCounter = Read16(IRQVector);
        }

        private void OpcodeNOP(ushort address, AddressingMode mode)
        {
            if (mode != AddressingMode.Implied)
                Read8(address);
            else
            {
                // No-op
                // .
                // ..
                // ...
                // ....
                // ..... so what are you doing still reading this?
            }
        }

        private void OpcodeBIT(ushort address, AddressingMode mode)
        {
            var value = Read8(address);

            _overflowFlag = (value & 0x40) != 0;
            _negativeFlag = (value & 0x80) != 0;
            _zeroFlag = (value & _aRegister) == 0;
        }

        private void OpcodeLAX(ushort address, AddressingMode mode)
        {
            _aRegister = Read8(address);
            _xRegister = _aRegister;

            SetNZ(_xRegister);
        }

        private void OpcodeSAX(ushort address, AddressingMode mode)
        {
            Write8(address, (byte)(_aRegister & _xRegister));
        }

        private void OpcodeDCP(ushort address, AddressingMode mode)
        {
            var value = (byte)(Read8(address) - 1);

            Write8(address, value);
            Compare(_aRegister, value);
        }

        private void OpcodeISC(ushort address, AddressingMode mode)
        {
            var value = (byte)(Read8(address) + 1);

            Write8(address, value);

            var total = _aRegister - value - (_carryFlag ? 0 : 1);

            _overflowFlag = ((_aRegister ^ value) & (_aRegister ^ total) & 0x80) != 0;
            _carryFlag = total >= 0;
            _aRegister = (byte)total;

            SetNZ(_aRegister);
        }

        private void OpcodeRLA(ushort address, AddressingMode mode)
        {
            var value = Read8(address);
            var carry = _carryFlag;
            _carryFlag = (value >> 7) != 0;
            value = (byte)((value << 1) | (carry ? 0x01 : 0x00));

            Write8(address, value);

            _aRegister &= value;

            SetNZ(_aRegister);
        }

        private void OpcodeRRA(ushort address, AddressingMode mode)
        {
            var value = Read8(address);
            var carry = _carryFlag;
            _carryFlag = (value & 0x01) != 0;
            value = (byte)((value >> 1) | (carry ? 0x80 : 0x00));

            Write8(address, value);

            var total = _aRegister + value + (_carryFlag ? 1 : 0);

            _overflowFlag = ((_aRegister ^ total) & (value ^ total) & 0x80) != 0;
            _carryFlag = total > 0xFF;
            _aRegister = (byte)total;

            SetNZ(_aRegister);
        }

        private void OpcodeSLO(ushort address, AddressingMode mode)
        {
            var value = Read8(address);
            _carryFlag = (value >> 7) != 0;
            value <<= 1;

            Write8(address, value);

            _aRegister |= value;

            SetNZ(_aRegister);
        }

        private void OpcodeSRE(ushort address, AddressingMode mode)
        {
            var value = Read8(address);
            _carryFlag = (value & 0x01) != 0;
            value >>= 1;

            Write8(address, value);

            _aRegister ^= Read8(address);

            SetNZ(_aRegister);
        }
        #endregion
        #endregion
    }
}
