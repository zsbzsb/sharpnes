﻿namespace SharpNES
{
    internal enum Opcodes
    {
        [OpcodeData(Code = 0xA9, Mode = AddressingMode.Immediate, Cycles = 2)]
        [OpcodeData(Code = 0xA5, Mode = AddressingMode.ZeroPage, Cycles = 3)]
        [OpcodeData(Code = 0xB5, Mode = AddressingMode.ZeroPageX, Cycles = 4)]
        [OpcodeData(Code = 0xAD, Mode = AddressingMode.Absolute, Cycles = 4)]
        [OpcodeData(Code = 0xBD, Mode = AddressingMode.AbsoluteX, Cycles = 4)]
        [OpcodeData(Code = 0xB9, Mode = AddressingMode.AbsoluteY, Cycles = 4)]
        [OpcodeData(Code = 0xA1, Mode = AddressingMode.IndirectX, Cycles = 6)]
        [OpcodeData(Code = 0xB1, Mode = AddressingMode.IndirectY, Cycles = 5)]
        LDA,

        [OpcodeData(Code = 0xA2, Mode = AddressingMode.Immediate, Cycles = 2)]
        [OpcodeData(Code = 0xA6, Mode = AddressingMode.ZeroPage, Cycles = 3)]
        [OpcodeData(Code = 0xB6, Mode = AddressingMode.ZeroPageY, Cycles = 4)]
        [OpcodeData(Code = 0xAE, Mode = AddressingMode.Absolute, Cycles = 4)]
        [OpcodeData(Code = 0xBE, Mode = AddressingMode.AbsoluteY, Cycles = 4)]
        LDX,

        [OpcodeData(Code = 0xA0, Mode = AddressingMode.Immediate, Cycles = 2)]
        [OpcodeData(Code = 0xA4, Mode = AddressingMode.ZeroPage, Cycles = 3)]
        [OpcodeData(Code = 0xB4, Mode = AddressingMode.ZeroPageX, Cycles = 4)]
        [OpcodeData(Code = 0xAC, Mode = AddressingMode.Absolute, Cycles = 4)]
        [OpcodeData(Code = 0xBC, Mode = AddressingMode.AbsoluteX, Cycles = 4)]
        LDY,

        [OpcodeData(Code = 0x85, Mode = AddressingMode.ZeroPage, Cycles = 3)]
        [OpcodeData(Code = 0x95, Mode = AddressingMode.ZeroPageX, Cycles = 4)]
        [OpcodeData(Code = 0x8D, Mode = AddressingMode.Absolute, Cycles = 4)]
        [OpcodeData(Code = 0x9D, Mode = AddressingMode.AbsoluteX, Cycles = 5, PageCrossPenalty = false)]
        [OpcodeData(Code = 0x99, Mode = AddressingMode.AbsoluteY, Cycles = 5, PageCrossPenalty = false)]
        [OpcodeData(Code = 0x81, Mode = AddressingMode.IndirectX, Cycles = 6)]
        [OpcodeData(Code = 0x91, Mode = AddressingMode.IndirectY, Cycles = 6, PageCrossPenalty = false)]
        STA,

        [OpcodeData(Code = 0x86, Mode = AddressingMode.ZeroPage, Cycles = 3)]
        [OpcodeData(Code = 0x96, Mode = AddressingMode.ZeroPageY, Cycles = 4)]
        [OpcodeData(Code = 0x8E, Mode = AddressingMode.Absolute, Cycles = 4)]
        STX,

        [OpcodeData(Code = 0x84, Mode = AddressingMode.ZeroPage, Cycles = 3)]
        [OpcodeData(Code = 0x94, Mode = AddressingMode.ZeroPageX, Cycles = 4)]
        [OpcodeData(Code = 0x8C, Mode = AddressingMode.Absolute, Cycles = 4)]
        STY,

        [OpcodeData(Code = 0xAA, Mode = AddressingMode.Implied, Cycles = 2)]
        TAX,

        [OpcodeData(Code = 0xA8, Mode = AddressingMode.Implied, Cycles = 2)]
        TAY,

        [OpcodeData(Code = 0xBA, Mode = AddressingMode.Implied, Cycles = 2)]
        TSX,

        [OpcodeData(Code = 0x8A, Mode = AddressingMode.Implied, Cycles = 2)]
        TXA,

        [OpcodeData(Code = 0x9A, Mode = AddressingMode.Implied, Cycles = 2)]
        TXS,

        [OpcodeData(Code = 0x98, Mode = AddressingMode.Implied, Cycles = 2)]
        TYA,

        [OpcodeData(Code = 0x18, Mode = AddressingMode.Implied, Cycles = 2)]
        CLC,

        [OpcodeData(Code = 0xD8, Mode = AddressingMode.Implied, Cycles = 2)]
        CLD,

        [OpcodeData(Code = 0x58, Mode = AddressingMode.Implied, Cycles = 2)]
        CLI,

        [OpcodeData(Code = 0xB8, Mode = AddressingMode.Implied, Cycles = 2)]
        CLV,

        [OpcodeData(Code = 0x38, Mode = AddressingMode.Implied, Cycles = 2)]
        SEC,

        [OpcodeData(Code = 0xF8, Mode = AddressingMode.Implied, Cycles = 2)]
        SED,

        [OpcodeData(Code = 0x78, Mode = AddressingMode.Implied, Cycles = 2)]
        SEI,

        [OpcodeData(Code = 0x10, Mode = AddressingMode.Relative, Cycles = 2)]
        BPL,

        [OpcodeData(Code = 0x30, Mode = AddressingMode.Relative, Cycles = 2)]
        BMI,

        [OpcodeData(Code = 0x50, Mode = AddressingMode.Relative, Cycles = 2)]
        BVC,

        [OpcodeData(Code = 0x70, Mode = AddressingMode.Relative, Cycles = 2)]
        BVS,

        [OpcodeData(Code = 0x90, Mode = AddressingMode.Relative, Cycles = 2)]
        BCC,

        [OpcodeData(Code = 0xB0, Mode = AddressingMode.Relative, Cycles = 2)]
        BCS,

        [OpcodeData(Code = 0xD0, Mode = AddressingMode.Relative, Cycles = 2)]
        BNE,

        [OpcodeData(Code = 0xF0, Mode = AddressingMode.Relative, Cycles = 2)]
        BEQ,

        [OpcodeData(Code = 0xCA, Mode = AddressingMode.Implied, Cycles = 2)]
        DEX,

        [OpcodeData(Code = 0x88, Mode = AddressingMode.Implied, Cycles = 2)]
        DEY,

        [OpcodeData(Code = 0xE8, Mode = AddressingMode.Implied, Cycles = 2)]
        INX,

        [OpcodeData(Code = 0xC8, Mode = AddressingMode.Implied, Cycles = 2)]
        INY,

        [OpcodeData(Code = 0xC6, Mode = AddressingMode.ZeroPage, Cycles = 5)]
        [OpcodeData(Code = 0xD6, Mode = AddressingMode.ZeroPageX, Cycles = 6)]
        [OpcodeData(Code = 0xCE, Mode = AddressingMode.Absolute, Cycles = 6)]
        [OpcodeData(Code = 0xDE, Mode = AddressingMode.AbsoluteX, Cycles = 7, PageCrossPenalty = false)]
        DEC,

        [OpcodeData(Code = 0xE6, Mode = AddressingMode.ZeroPage, Cycles = 5)]
        [OpcodeData(Code = 0xF6, Mode = AddressingMode.ZeroPageX, Cycles = 6)]
        [OpcodeData(Code = 0xEE, Mode = AddressingMode.Absolute, Cycles = 6)]
        [OpcodeData(Code = 0xFE, Mode = AddressingMode.AbsoluteX, Cycles = 7, PageCrossPenalty = false)]
        INC,

        [OpcodeData(Code = 0xC9, Mode = AddressingMode.Immediate, Cycles = 2)]
        [OpcodeData(Code = 0xC5, Mode = AddressingMode.ZeroPage, Cycles = 3)]
        [OpcodeData(Code = 0xD5, Mode = AddressingMode.ZeroPageX, Cycles = 4)]
        [OpcodeData(Code = 0xCD, Mode = AddressingMode.Absolute, Cycles = 4)]
        [OpcodeData(Code = 0xDD, Mode = AddressingMode.AbsoluteX, Cycles = 4)]
        [OpcodeData(Code = 0xD9, Mode = AddressingMode.AbsoluteY, Cycles = 4)]
        [OpcodeData(Code = 0xC1, Mode = AddressingMode.IndirectX, Cycles = 6)]
        [OpcodeData(Code = 0xD1, Mode = AddressingMode.IndirectY, Cycles = 5)]
        CMP,

        [OpcodeData(Code = 0xE0, Mode = AddressingMode.Immediate, Cycles = 2)]
        [OpcodeData(Code = 0xE4, Mode = AddressingMode.ZeroPage, Cycles = 3)]
        [OpcodeData(Code = 0xEC, Mode = AddressingMode.Absolute, Cycles = 4)]
        CPX,

        [OpcodeData(Code = 0xC0, Mode = AddressingMode.Immediate, Cycles = 2)]
        [OpcodeData(Code = 0xC4, Mode = AddressingMode.ZeroPage, Cycles = 3)]
        [OpcodeData(Code = 0xCC, Mode = AddressingMode.Absolute, Cycles = 4)]
        CPY,

        [OpcodeData(Code = 0x4C, Mode = AddressingMode.Absolute, Cycles = 3)]
        [OpcodeData(Code = 0x6C, Mode = AddressingMode.Indirect, Cycles = 5)]
        JMP,

        [OpcodeData(Code = 0x20, Mode = AddressingMode.Absolute, Cycles = 6)]
        JSR,

        [OpcodeData(Code = 0x40, Mode = AddressingMode.Implied, Cycles = 6)]
        RTI,

        [OpcodeData(Code = 0x60, Mode = AddressingMode.Implied, Cycles = 6)]
        RTS,

        [OpcodeData(Code = 0x29, Mode = AddressingMode.Immediate, Cycles = 2)]
        [OpcodeData(Code = 0x25, Mode = AddressingMode.ZeroPage, Cycles = 2)]
        [OpcodeData(Code = 0x35, Mode = AddressingMode.ZeroPageX, Cycles = 3)]
        [OpcodeData(Code = 0x2D, Mode = AddressingMode.Absolute, Cycles = 4)]
        [OpcodeData(Code = 0x3D, Mode = AddressingMode.AbsoluteX, Cycles = 4)]
        [OpcodeData(Code = 0x39, Mode = AddressingMode.AbsoluteY, Cycles = 4)]
        [OpcodeData(Code = 0x21, Mode = AddressingMode.IndirectX, Cycles = 6)]
        [OpcodeData(Code = 0x31, Mode = AddressingMode.IndirectY, Cycles = 5)]
        AND,

        [OpcodeData(Code = 0x49, Mode = AddressingMode.Immediate, Cycles = 2)]
        [OpcodeData(Code = 0x45, Mode = AddressingMode.ZeroPage, Cycles = 3)]
        [OpcodeData(Code = 0x55, Mode = AddressingMode.ZeroPageX, Cycles = 4)]
        [OpcodeData(Code = 0x4D, Mode = AddressingMode.Absolute, Cycles = 4)]
        [OpcodeData(Code = 0x5D, Mode = AddressingMode.AbsoluteX, Cycles = 4)]
        [OpcodeData(Code = 0x59, Mode = AddressingMode.AbsoluteY, Cycles = 4)]
        [OpcodeData(Code = 0x41, Mode = AddressingMode.IndirectX, Cycles = 6)]
        [OpcodeData(Code = 0x51, Mode = AddressingMode.IndirectY, Cycles = 5)]
        EOR,

        [OpcodeData(Code = 0x09, Mode = AddressingMode.Immediate, Cycles = 2)]
        [OpcodeData(Code = 0x05, Mode = AddressingMode.ZeroPage, Cycles = 2)]
        [OpcodeData(Code = 0x15, Mode = AddressingMode.ZeroPageX, Cycles = 3)]
        [OpcodeData(Code = 0x0D, Mode = AddressingMode.Absolute, Cycles = 4)]
        [OpcodeData(Code = 0x1D, Mode = AddressingMode.AbsoluteX, Cycles = 4)]
        [OpcodeData(Code = 0x19, Mode = AddressingMode.AbsoluteY, Cycles = 4)]
        [OpcodeData(Code = 0x01, Mode = AddressingMode.IndirectX, Cycles = 6)]
        [OpcodeData(Code = 0x11, Mode = AddressingMode.IndirectY, Cycles = 5)]
        ORA,

        [OpcodeData(Code = 0x0A, Mode = AddressingMode.Accumulator, Cycles = 2)]
        [OpcodeData(Code = 0x06, Mode = AddressingMode.ZeroPage, Cycles = 5)]
        [OpcodeData(Code = 0x16, Mode = AddressingMode.ZeroPageX, Cycles = 6)]
        [OpcodeData(Code = 0x0E, Mode = AddressingMode.Absolute, Cycles = 6)]
        [OpcodeData(Code = 0x1E, Mode = AddressingMode.AbsoluteX, Cycles = 7, PageCrossPenalty = false)]
        ASL,

        [OpcodeData(Code = 0x4A, Mode = AddressingMode.Accumulator, Cycles = 2)]
        [OpcodeData(Code = 0x46, Mode = AddressingMode.ZeroPage, Cycles = 5)]
        [OpcodeData(Code = 0x56, Mode = AddressingMode.ZeroPageX, Cycles = 6)]
        [OpcodeData(Code = 0x4E, Mode = AddressingMode.Absolute, Cycles = 6)]
        [OpcodeData(Code = 0x5E, Mode = AddressingMode.AbsoluteX, Cycles = 7, PageCrossPenalty = false)]
        LSR,

        [OpcodeData(Code = 0x2A, Mode = AddressingMode.Accumulator, Cycles = 2)]
        [OpcodeData(Code = 0x26, Mode = AddressingMode.ZeroPage, Cycles = 5)]
        [OpcodeData(Code = 0x36, Mode = AddressingMode.ZeroPageX, Cycles = 6)]
        [OpcodeData(Code = 0x2E, Mode = AddressingMode.Absolute, Cycles = 6)]
        [OpcodeData(Code = 0x3E, Mode = AddressingMode.AbsoluteX, Cycles = 7, PageCrossPenalty = false)]
        ROL,

        [OpcodeData(Code = 0x6A, Mode = AddressingMode.Accumulator, Cycles = 2)]
        [OpcodeData(Code = 0x66, Mode = AddressingMode.ZeroPage, Cycles = 5)]
        [OpcodeData(Code = 0x76, Mode = AddressingMode.ZeroPageX, Cycles = 6)]
        [OpcodeData(Code = 0x6E, Mode = AddressingMode.Absolute, Cycles = 6)]
        [OpcodeData(Code = 0x7E, Mode = AddressingMode.AbsoluteX, Cycles = 7, PageCrossPenalty = false)]
        ROR,

        [OpcodeData(Code = 0x69, Mode = AddressingMode.Immediate, Cycles = 2)]
        [OpcodeData(Code = 0x65, Mode = AddressingMode.ZeroPage, Cycles = 3)]
        [OpcodeData(Code = 0x75, Mode = AddressingMode.ZeroPageX, Cycles = 4)]
        [OpcodeData(Code = 0x6D, Mode = AddressingMode.Absolute, Cycles = 4)]
        [OpcodeData(Code = 0x7D, Mode = AddressingMode.AbsoluteX, Cycles = 4)]
        [OpcodeData(Code = 0x79, Mode = AddressingMode.AbsoluteY, Cycles = 4)]
        [OpcodeData(Code = 0x61, Mode = AddressingMode.IndirectX, Cycles = 6)]
        [OpcodeData(Code = 0x71, Mode = AddressingMode.IndirectY, Cycles = 5)]
        ADC,

        [OpcodeData(Code = 0xE9, Mode = AddressingMode.Immediate, Cycles = 2)]
        [OpcodeData(Code = 0xEB, Mode = AddressingMode.Immediate, Cycles = 2)]
        [OpcodeData(Code = 0xE5, Mode = AddressingMode.ZeroPage, Cycles = 3)]
        [OpcodeData(Code = 0xF5, Mode = AddressingMode.ZeroPageX, Cycles = 4)]
        [OpcodeData(Code = 0xED, Mode = AddressingMode.Absolute, Cycles = 4)]
        [OpcodeData(Code = 0xFD, Mode = AddressingMode.AbsoluteX, Cycles = 4)]
        [OpcodeData(Code = 0xF9, Mode = AddressingMode.AbsoluteY, Cycles = 4)]
        [OpcodeData(Code = 0xE1, Mode = AddressingMode.IndirectX, Cycles = 6)]
        [OpcodeData(Code = 0xF1, Mode = AddressingMode.IndirectY, Cycles = 5)]
        SBC,

        [OpcodeData(Code = 0x48, Mode = AddressingMode.Implied, Cycles = 3)]
        PHA,

        [OpcodeData(Code = 0x08, Mode = AddressingMode.Implied, Cycles = 3)]
        PHP,

        [OpcodeData(Code = 0x68, Mode = AddressingMode.Implied, Cycles = 4)]
        PLA,

        [OpcodeData(Code = 0x28, Mode = AddressingMode.Implied, Cycles = 4)]
        PLP,

        [OpcodeData(Code = 0x00, Mode = AddressingMode.Implied, Cycles = 7)]
        BRK,

        [OpcodeData(Code = 0x1A, Mode = AddressingMode.Implied, Cycles = 2)]
        [OpcodeData(Code = 0x3A, Mode = AddressingMode.Implied, Cycles = 2)]
        [OpcodeData(Code = 0x5A, Mode = AddressingMode.Implied, Cycles = 2)]
        [OpcodeData(Code = 0x7A, Mode = AddressingMode.Implied, Cycles = 2)]
        [OpcodeData(Code = 0xDA, Mode = AddressingMode.Implied, Cycles = 2)]
        [OpcodeData(Code = 0xEA, Mode = AddressingMode.Implied, Cycles = 2)]
        [OpcodeData(Code = 0xFA, Mode = AddressingMode.Implied, Cycles = 2)]
        [OpcodeData(Code = 0x80, Mode = AddressingMode.Immediate, Cycles = 2)]
        [OpcodeData(Code = 0x82, Mode = AddressingMode.Immediate, Cycles = 2)]
        [OpcodeData(Code = 0x89, Mode = AddressingMode.Immediate, Cycles = 2)]
        [OpcodeData(Code = 0xC2, Mode = AddressingMode.Immediate, Cycles = 2)]
        [OpcodeData(Code = 0xE2, Mode = AddressingMode.Immediate, Cycles = 2)]
        [OpcodeData(Code = 0x0C, Mode = AddressingMode.Absolute, Cycles = 4)]
        [OpcodeData(Code = 0x1C, Mode = AddressingMode.AbsoluteX, Cycles = 4)]
        [OpcodeData(Code = 0x3C, Mode = AddressingMode.AbsoluteX, Cycles = 4)]
        [OpcodeData(Code = 0x5C, Mode = AddressingMode.AbsoluteX, Cycles = 4)]
        [OpcodeData(Code = 0x7C, Mode = AddressingMode.AbsoluteX, Cycles = 4)]
        [OpcodeData(Code = 0xDC, Mode = AddressingMode.AbsoluteX, Cycles = 4)]
        [OpcodeData(Code = 0xFC, Mode = AddressingMode.AbsoluteX, Cycles = 4)]
        [OpcodeData(Code = 0x04, Mode = AddressingMode.ZeroPage, Cycles = 3)]
        [OpcodeData(Code = 0x44, Mode = AddressingMode.ZeroPage, Cycles = 3)]
        [OpcodeData(Code = 0x64, Mode = AddressingMode.ZeroPage, Cycles = 3)]
        [OpcodeData(Code = 0x14, Mode = AddressingMode.ZeroPageX, Cycles = 4)]
        [OpcodeData(Code = 0x34, Mode = AddressingMode.ZeroPageX, Cycles = 4)]
        [OpcodeData(Code = 0x54, Mode = AddressingMode.ZeroPageX, Cycles = 4)]
        [OpcodeData(Code = 0x74, Mode = AddressingMode.ZeroPageX, Cycles = 4)]
        [OpcodeData(Code = 0xD4, Mode = AddressingMode.ZeroPageX, Cycles = 4)]
        [OpcodeData(Code = 0xF4, Mode = AddressingMode.ZeroPageX, Cycles = 4)]
        NOP,

        [OpcodeData(Code = 0x24, Mode = AddressingMode.ZeroPage, Cycles = 3)]
        [OpcodeData(Code = 0x2C, Mode = AddressingMode.Absolute, Cycles = 4)]
        BIT,

        [OpcodeData(Code = 0xA7, Mode = AddressingMode.ZeroPage, Cycles = 3)]
        [OpcodeData(Code = 0xB7, Mode = AddressingMode.ZeroPageY, Cycles = 4)]
        [OpcodeData(Code = 0xAF, Mode = AddressingMode.Absolute, Cycles = 4)]
        [OpcodeData(Code = 0xBF, Mode = AddressingMode.AbsoluteY, Cycles = 4)]
        [OpcodeData(Code = 0xA3, Mode = AddressingMode.IndirectX, Cycles = 6)]
        [OpcodeData(Code = 0xB3, Mode = AddressingMode.IndirectY, Cycles = 5)]
        LAX,

        [OpcodeData(Code = 0x87, Mode = AddressingMode.ZeroPage, Cycles = 3)]
        [OpcodeData(Code = 0x97, Mode = AddressingMode.ZeroPageY, Cycles = 4)]
        [OpcodeData(Code = 0x8F, Mode = AddressingMode.Absolute, Cycles = 4)]
        [OpcodeData(Code = 0x83, Mode = AddressingMode.IndirectX, Cycles = 6)]
        SAX,

        [OpcodeData(Code = 0xC7, Mode = AddressingMode.ZeroPage, Cycles = 5)]
        [OpcodeData(Code = 0xD7, Mode = AddressingMode.ZeroPageX, Cycles = 6)]
        [OpcodeData(Code = 0xCF, Mode = AddressingMode.Absolute, Cycles = 6)]
        [OpcodeData(Code = 0xDF, Mode = AddressingMode.AbsoluteX, Cycles = 7)]
        [OpcodeData(Code = 0xDB, Mode = AddressingMode.AbsoluteY, Cycles = 7)]
        [OpcodeData(Code = 0xC3, Mode = AddressingMode.IndirectX, Cycles = 8)]
        [OpcodeData(Code = 0xD3, Mode = AddressingMode.IndirectY, Cycles = 8)]
        DCP,

        [OpcodeData(Code = 0xE7, Mode = AddressingMode.ZeroPage, Cycles = 5)]
        [OpcodeData(Code = 0xF7, Mode = AddressingMode.ZeroPageX, Cycles = 6)]
        [OpcodeData(Code = 0xEF, Mode = AddressingMode.Absolute, Cycles = 6)]
        [OpcodeData(Code = 0xFF, Mode = AddressingMode.AbsoluteX, Cycles = 7)]
        [OpcodeData(Code = 0xFB, Mode = AddressingMode.AbsoluteY, Cycles = 7)]
        [OpcodeData(Code = 0xE3, Mode = AddressingMode.IndirectX, Cycles = 8)]
        [OpcodeData(Code = 0xF3, Mode = AddressingMode.IndirectY, Cycles = 8)]
        ISC,

        [OpcodeData(Code = 0x27, Mode = AddressingMode.ZeroPage, Cycles = 5)]
        [OpcodeData(Code = 0x37, Mode = AddressingMode.ZeroPageX, Cycles = 6)]
        [OpcodeData(Code = 0x2F, Mode = AddressingMode.Absolute, Cycles = 6)]
        [OpcodeData(Code = 0x3F, Mode = AddressingMode.AbsoluteX, Cycles = 7)]
        [OpcodeData(Code = 0x3B, Mode = AddressingMode.AbsoluteY, Cycles = 7)]
        [OpcodeData(Code = 0x23, Mode = AddressingMode.IndirectX, Cycles = 8)]
        [OpcodeData(Code = 0x33, Mode = AddressingMode.IndirectY, Cycles = 8)]
        RLA,

        [OpcodeData(Code = 0x67, Mode = AddressingMode.ZeroPage, Cycles = 5)]
        [OpcodeData(Code = 0x77, Mode = AddressingMode.ZeroPageX, Cycles = 6)]
        [OpcodeData(Code = 0x6F, Mode = AddressingMode.Absolute, Cycles = 6)]
        [OpcodeData(Code = 0x7F, Mode = AddressingMode.AbsoluteX, Cycles = 7)]
        [OpcodeData(Code = 0x7B, Mode = AddressingMode.AbsoluteY, Cycles = 7)]
        [OpcodeData(Code = 0x63, Mode = AddressingMode.IndirectX, Cycles = 8)]
        [OpcodeData(Code = 0x73, Mode = AddressingMode.IndirectY, Cycles = 8)]
        RRA,

        [OpcodeData(Code = 0x07, Mode = AddressingMode.ZeroPage, Cycles = 5)]
        [OpcodeData(Code = 0x17, Mode = AddressingMode.ZeroPageX, Cycles = 6)]
        [OpcodeData(Code = 0x0F, Mode = AddressingMode.Absolute, Cycles = 6)]
        [OpcodeData(Code = 0x1F, Mode = AddressingMode.AbsoluteX, Cycles = 7)]
        [OpcodeData(Code = 0x1B, Mode = AddressingMode.AbsoluteY, Cycles = 7)]
        [OpcodeData(Code = 0x03, Mode = AddressingMode.IndirectX, Cycles = 8)]
        [OpcodeData(Code = 0x13, Mode = AddressingMode.IndirectY, Cycles = 8)]
        SLO,

        [OpcodeData(Code = 0x47, Mode = AddressingMode.ZeroPage, Cycles = 5)]
        [OpcodeData(Code = 0x57, Mode = AddressingMode.ZeroPageX, Cycles = 6)]
        [OpcodeData(Code = 0x4F, Mode = AddressingMode.Absolute, Cycles = 6)]
        [OpcodeData(Code = 0x5F, Mode = AddressingMode.AbsoluteX, Cycles = 7)]
        [OpcodeData(Code = 0x5B, Mode = AddressingMode.AbsoluteY, Cycles = 7)]
        [OpcodeData(Code = 0x43, Mode = AddressingMode.IndirectX, Cycles = 8)]
        [OpcodeData(Code = 0x53, Mode = AddressingMode.IndirectY, Cycles = 8)]
        SRE,
    }
}
