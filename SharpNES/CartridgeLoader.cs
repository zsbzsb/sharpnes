﻿using System;
using System.IO;
using System.Text;

namespace SharpNES
{
    public static class CartridgeLoader
    {
        #region Functions
        public static Cartridge LoadFromFile(string path)
        {
            try
            {
                using (var file = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    var headerbuffer = new byte[16];
                    file.Read(headerbuffer, 0, headerbuffer.Length);

                    if (Encoding.ASCII.GetString(headerbuffer, 0, 3) != "NES")
                        return null;

                    if (headerbuffer[3] != 0x1A)
                        return null;

                    byte prgCount = headerbuffer[4];
                    byte chrCount = headerbuffer[5];

                    byte controlFlag1 = headerbuffer[6];
                    byte controlFlag2 = headerbuffer[7];

                    byte mapperID = (byte)((controlFlag1 >> 4) | (controlFlag2 & 0xF0));
                    MirroringMode mode = (controlFlag1 & 0x08) != 0 ? MirroringMode.FourScreen : (controlFlag1 & 0x01) != 0 ? MirroringMode.Vertical : MirroringMode.Horizontal;
                    bool sramEnabled = (controlFlag1 & 2) != 0;
                    bool trainer = (controlFlag1 & 4) != 0;

                    if (trainer)
                        file.Position += 512;

                    var prgROM = new byte[prgCount][];
                    for (int i = 0; i < prgCount; i++)
                    {
                        prgROM[i] = new byte[0x4000];
                        file.Read(prgROM[i], 0, prgROM[i].Length);
                    }

                    var chrROM = new byte[chrCount][];
                    for (int i = 0; i < chrCount; i++)
                    {
                        chrROM[i] = new byte[0x2000];
                        file.Read(chrROM[i], 0, chrROM[i].Length);
                    }

                    return new Cartridge(prgROM, chrROM, mapperID, mode, sramEnabled);
                }
            }
            catch
            {
                throw new Exception("Failed to load Cartridge");
            }
        }
        #endregion
    }
}
