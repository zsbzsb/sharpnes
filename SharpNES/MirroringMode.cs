﻿namespace SharpNES
{
    internal enum MirroringMode
    {
        Horizontal,
        Vertical,
        FourScreen
    }
}
