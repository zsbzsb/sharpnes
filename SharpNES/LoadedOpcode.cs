﻿namespace SharpNES
{
    internal class LoadedOpcode
    {
        #region Properties
        public Opcodes Opcode { get; }

        public OpcodeData Data { get; }
        #endregion

        #region CTOR
        public LoadedOpcode(Opcodes opcode, OpcodeData data)
        {
            Opcode = opcode;
            Data = data;
        }
        #endregion
    }
}
