﻿namespace SharpNES
{
    public class NESConsole
    {
        #region Constants
        private const ushort CartridgeStart = 0x4020;
        private const ushort CartridgeEnd = 0xFFFF;
        #endregion

        #region Variables
        private Bus _bus;
        private Cartridge _cartridge;
        private RAM _ram;
        private CPU _cpu;
        private PPU _ppu;
        private InputManager _input;
        #endregion

        #region CTOR
        public NESConsole(Cartridge cartridge)
        {
            _cartridge = cartridge;

            _bus = new Bus();
            _ram = new RAM(_bus);
            _cpu = new CPU(_bus);
            _ppu = new PPU(_bus, cartridge);
            _input = new InputManager();

            _bus.MapReadLocationRange(CartridgeStart, CartridgeEnd, _cartridge.Mapper.ReadCPU);
            _bus.MapWriteLocationRange(CartridgeStart, CartridgeEnd, _cartridge.Mapper.WriteCPU);

            Reset();
        }
        #endregion

        #region Functions
        public void Reset()
        {
            _ram.Reset();
            _cpu.Reset();
            _ppu.Reset();
        }

        public void Step()
        {
            var cpuCycles = _cpu.Step();

            for (int i = 0; i < cpuCycles * 3; i++)
                _ppu.Step();
        }

        public void Run()
        {
            for (; ; )
                Step();
        }
        #endregion
    }
}
