﻿using System;

namespace SharpNES
{
    internal class PPU
    {
        #region Constants
        private const ushort PPUStart = 0x2000;
        private const ushort PPUEnd = 0x3FFF;
        private const ushort PatternTableStart = 0x0000;
        private const ushort NameTableStart = 0x2000;
        private const ushort PaletteStart = 0x3F00;
        private const ushort PaletteEnd = 0x3FFF;
        private const ushort BusSize = 0x4000;
        #endregion

        #region Variables
        // Execution State
        private uint _cycleCount = 0; // 0 is idle, 1-256 is rendering, 257-340 is fetching data
        private uint _scanline = 261; // 0-239 is visible, 240 is post, 241-260 is vblank, 261 is pre

        // VBlank State
        private bool _vBlankStarted = false;

        // Control Register
        private ushort _nametable = 0;
        private byte _increment = 1;
        private ushort _spriteTable = 0;
        private ushort _backgroundTable = 0;
        private bool _largeSprites = false;
        private bool _slaveToggle = false;
        private bool _generateNMI = false;

        // Mask Register
        private bool _useGrayscale;
        private bool _showLeftmostBackground = false;
        private bool _showLeftmostSprites = false;
        private bool _showBackground = false;
        private bool _showSprites = false;
        private bool _emphasizeRed = false;
        private bool _emphasizeGreen = false;
        private bool _emphasizeBlue = false;

        // Status Register
        private bool _spriteHit = false;
        private bool _spriteOverflow = false;

        // Storage
        private byte[] _paletteData = new byte[32];
        private byte[] _nametableData = new byte[2048];
        private byte[] _oamData = new byte[256];
        private Cartridge _cartridge;

        private byte _oamAddress = 0;

        private byte _latch = 0;
        private ushort _v = 0;
        private ushort _t = 0;
        private byte _x = 0;
        private bool _writeToggle = false;
        private byte _dataBuffer = 0;

        private int[,] _screenBuffer = new int[240, 256];
        private Bus _bus = new Bus(BusSize);

        private static int[][] _nametableMap = new int[3][];
        #endregion

        #region CTOR
        static PPU()
        {
            _nametableMap[(int)MirroringMode.Horizontal] = new int[] { 0, 0, 1, 1 };
            _nametableMap[(int)MirroringMode.Vertical] = new int[] { 0, 1, 0, 1 };
            _nametableMap[(int)MirroringMode.FourScreen] = new int[] { 0, 1, 2, 3 };
        }

        public PPU(Bus cpuBus, Cartridge cartridge)
        {
            _cartridge = cartridge;

            if (_cartridge.Mode == MirroringMode.FourScreen)
                throw new Exception("FourScreen mirroring is not supported");

            cpuBus.MapReadLocationRange(PPUStart, PPUEnd, new ReadDelegate[]{
                null,        // $2000
                null,        // $2001
                ReadStatus,  // $2002
                null,        // $2003
                ReadOAMData, // $2004
                null,        // $2005
                null,        // $2006
                ReadData     // $2007
            });

            cpuBus.MapWriteLocationRange(PPUStart, PPUEnd, new WriteDelegate[]{
                WriteControl,    // $2000
                WriteMask,       // $2001
                null,            // $2002
                WriteOAMAddress, // $2003
                WriteOAMData,    // $2004
                WriteScroll,     // $2005
                WriteAddress,    // $2006
                WriteData        // $2007
            });

            _bus.MapReadLocationRange(PatternTableStart, NameTableStart - 1, _cartridge.Mapper.ReadPPU);
            _bus.MapReadLocationRange(NameTableStart, PaletteStart - 1, ReadNametable);
            _bus.MapReadLocationRange(PaletteStart, PaletteEnd, ReadPalette);

            _bus.MapWriteLocationRange(PatternTableStart, NameTableStart - 1, _cartridge.Mapper.WritePPU);
            _bus.MapWriteLocationRange(NameTableStart, PaletteStart - 1, WriteNametable);
            _bus.MapWriteLocationRange(PaletteStart, PaletteEnd, WritePalette);
        }
        #endregion

        #region Functions
        public void Reset()
        {
            // TODO reset the PPU
        }

        public void Step()
        {
            var renderingEnabled = _showBackground || _showSprites;
            var visibleCycle = _cycleCount >= 1 && _cycleCount <= 256;
            var fetchCycle = visibleCycle || (_cycleCount >= 321 && _cycleCount <= 336);
            var visibleLine = _scanline <= 239;
            var preLine = _scanline == 261;

            if (renderingEnabled)
            {
                if (visibleCycle && visibleLine)
                    RenderPixel();

                if (fetchCycle && (visibleLine || preLine))
                {
                    switch (_cycleCount % 8)
                    {
                        case 2:
                            FetchNametableData();
                            break;

                        case 4:
                            FetchAttributeData();
                            break;

                        case 6:
                            FetchTileBitmap(true);
                            break;

                        case 0:
                            FetchTileBitmap(false);
                            break;

                    }
                }

            }

            _cycleCount++;
            if (_cycleCount == 341)
            {
                _cycleCount = 0;
                _scanline++;

                if (_scanline == 262)
                {
                    _scanline = 0;
                }
            }
        }

        private void RenderPixel()
        {

        }

        private void FetchNametableData()
        {

        }

        private void FetchAttributeData()
        {

        }

        private void FetchTileBitmap(bool LowByte)
        {

        }

        private void WriteControl(ushort address, byte value)
        {
            _latch = value;

            _t = (ushort)((_t & 0xF3FF) | ((value & 3) << 10));

            var nametableValue = (value & 3);
            if (nametableValue == 0)
                _nametable = 0x2000;
            else if (nametableValue == 1)
                _nametable = 0x2400;
            else if (nametableValue == 2)
                _nametable = 0x2800;
            else if (nametableValue == 3)
                _nametable = 0x2C00;

            _increment = (value & (1 << 2)) != 0 ? (byte)32 : (byte)1;
            _spriteTable = (value & (1 << 3)) != 0 ? (ushort)0x1000 : (ushort)0;
            _backgroundTable = (value & (1 << 4)) != 0 ? (ushort)0x1000 : (ushort)0;
            _largeSprites = (value & (1 << 5)) != 0;
            _slaveToggle = (value & (1 << 6)) != 0;
            _generateNMI = (value & (1 << 7)) != 0;
        }

        private void WriteMask(ushort address, byte value)
        {
            _latch = value;

            _useGrayscale = (value & (1 << 0)) != 0;
            _showLeftmostBackground = (value & (1 << 1)) != 0;
            _showLeftmostSprites = (value & (1 << 2)) != 0;
            _showBackground = (value & (1 << 3)) != 0;
            _showSprites = (value & (1 << 4)) != 0;
            _emphasizeRed = (value & (1 << 5)) != 0;
            _emphasizeGreen = (value & (1 << 6)) != 0;
            _emphasizeBlue = (value & (1 << 7)) != 0;
        }

        private void WriteOAMAddress(ushort address, byte value)
        {
            _latch = value;
            _oamAddress = value;
        }

        private void WriteOAMData(ushort address, byte value)
        {
            _latch = value;
            // TODO skip writing during rendering
            _oamData[_oamAddress++] = value;
        }

        private void WriteScroll(ushort address, byte value)
        {
            _latch = value;

            if (!_writeToggle)
            {
                _t = (ushort)((_t & 0xFFE0) | (value >> 3));
                _x = (byte)(value & 0x07);
                _writeToggle = true;
            }
            else
            {
                _t = (ushort)((_t & 0x8FFF) | ((value & 0x07) << 12));
                _t = (ushort)((_t & 0xFC1F) | ((value & 0xF8) << 2));
                _writeToggle = false;
            }
        }

        private void WriteAddress(ushort address, byte value)
        {
            _latch = value;

            if (!_writeToggle)
            {
                _t = (ushort)((_t & 0x80FF) | ((value & 0x3F) << 8));
                _writeToggle = true;
            }
            else
            {
                _t = (ushort)((_t & 0xFF00) | value);
                _v = _t;
                _writeToggle = false;
            }
        }

        private void WriteData(ushort address, byte value)
        {
            _latch = value;
            _bus.Write((ushort)(_v % BusSize), value);
            _v += _increment;
        }

        private byte ReadStatus(ushort address)
        {
            byte status = (byte)(_latch & 0x1F);

            if (_spriteOverflow)
                status |= (byte)(1 << 5);
            if (_spriteHit)
                status |= (byte)(1 << 6);
            if (_vBlankStarted)
                status |= (byte)(1 << 7);

            _writeToggle = false;
            _vBlankStarted = false;

            return status;
        }

        private byte ReadOAMData(ushort address)
        {
            return _oamData[_oamAddress];
        }

        private byte ReadData(ushort address)
        {
            var value = _dataBuffer;
            _dataBuffer = _bus.Read((ushort)(_v % BusSize));

            if (_v % BusSize >= PaletteStart)
                value = _dataBuffer;

            _v += _increment;

            return value;
        }

        private void WriteNametable(ushort address, byte value)
        {
            _nametableData[FindNametableIndex(address)] = value;
        }

        private void WritePalette(ushort address, byte value)
        {
            _paletteData[FindPaletteIndex(address)] = (byte)(value & 0x3F);
        }

        private byte ReadNametable(ushort address)
        {
            return _nametableData[FindNametableIndex(address)];
        }

        private byte ReadPalette(ushort address)
        {
            return _paletteData[FindPaletteIndex(address)];
        }

        private int FindPaletteIndex(ushort address)
        {
            address -= PaletteStart;

            if (address >= 0x10 && address % 0x04 == 0)
                address -= 0x10;

            return address;
        }

        private int FindNametableIndex(ushort address)
        {
            address -= NameTableStart;

            var table = address / 0x0400;
            var offset = address % 0x0400;

            return _nametableMap[(int)_cartridge.Mode][table] * 0x0400 + offset;
        }
        #endregion
    }
}
