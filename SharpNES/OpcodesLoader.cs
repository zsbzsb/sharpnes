﻿using System;
using System.Reflection;

namespace SharpNES
{
    internal static class OpcodesLoader
    {
        #region Variables
        private static LoadedOpcode[] _opcodes;
        #endregion

        #region Constructors
        static OpcodesLoader()
        {
            _opcodes = new LoadedOpcode[0x100];

            foreach (Opcodes opcode in Enum.GetValues(typeof(Opcodes)))
            {
                foreach (OpcodeData data in opcode.GetType().GetMember(opcode.ToString())[0].GetCustomAttributes<OpcodeData>())
                {
                    if (_opcodes[data.Code] != null)
                        throw new Exception("Duplicate opcode value is defined in the Opcodes enum");

                    _opcodes[data.Code] = new LoadedOpcode(opcode, data);
                }
            }
        }
        #endregion

        #region Functions
        public static LoadedOpcode GetOpcode(byte code)
        {
            var opcode = _opcodes[code];

            if (opcode == null)
                throw new Exception("Non implemented opcode was requested");

            return opcode;
        }
        #endregion
    }
}
