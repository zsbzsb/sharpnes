﻿using System;

namespace SharpNES.Mappers
{
    // NROM Mapper
    // http://wiki.nesdev.com/w/index.php/NROM
    // this mapper doesn't do anything
    internal class Mapper0 : IMapper
    {
        #region Constants
        protected const ushort LowerPRGOffset = 0x8000;
        protected const ushort UpperPRGOffset = 0xC000;
        #endregion

        #region Variables
        private Cartridge _cartridge = null;
        private int _upperBank = 0;
        #endregion

        #region Functions
        public void Reset(Cartridge cartridge)
        {
            _cartridge = cartridge;

            if (_cartridge.ProgramROM.Length >= 2)
                _upperBank = 1;
        }

        public byte ReadCPU(ushort address)
        {
            if (address >= UpperPRGOffset)
                return _cartridge.ProgramROM[_upperBank][address - UpperPRGOffset];

            else if (address >= LowerPRGOffset)
                return _cartridge.ProgramROM[0][address - LowerPRGOffset];

            else
                throw new Exception("Unhandled memory read operation");
        }

        public void WriteCPU(ushort address, byte value)
        {
            // Nothing to do here
        }

        public byte ReadPPU(ushort address)
        {
            return _cartridge.CharacterROM[0][address];
        }

        public void WritePPU(ushort address, byte value)
        {
            // Nothing to do here
        }
        #endregion
    }
}
